# gk-chat

A Vue component for gk-chat into your application.

## Project setup

```
npm install --registry=https://registry.npm.taobao.org
```

### Compiles and hot-reloads for development

```
npm run dev
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

### Customize confi guration

See [Configuration Reference](https://cli.vuejs.org/config/).
