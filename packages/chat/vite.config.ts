import { loadEnv } from "vite";
import vue from "@vitejs/plugin-vue";
import AutoImport from "unplugin-auto-import/vite";
import Components from "unplugin-vue-components/vite";
import { ElementPlusResolver } from "unplugin-vue-components/resolvers";
import Icons from "unplugin-icons/vite";
import IconsResolver from "unplugin-icons/resolver";
import path from "path";
import VitePluginStyleInject from "vite-plugin-style-inject";
import type { UserConfig, ConfigEnv } from "vite";

const CWD = process.cwd();

export default ({ command, mode }: ConfigEnv): UserConfig => {
  {
    // 环境变量
    console.log(mode === "search");
    return {
      resolve: {
        alias: {
          "@": path.resolve(__dirname, "src"),
        },
      },
      plugins: [
        vue({
          template: {
            compilerOptions: {
              // isCustomElement: (tag: string[]) => {
              //   console.log(tag);
              // },
            },
          },
        }),

        AutoImport({
          imports: ["vue"],
          resolvers: [
            ElementPlusResolver({
              importStyle: "sass",
            }),
            IconsResolver({
              prefix: "Icon",
            }),
          ],
        }),
        Components({
          resolvers: [
            ElementPlusResolver({
              importStyle: "sass",
            }),
            IconsResolver({
              enabledCollections: ["ep"],
            }),
          ],
        }),
        Icons({
          autoInstall: true,
        }),
        VitePluginStyleInject(),
      ],
      build: {
        target: "esnext",
        minify: "terser",
        lib: {
          entry: mode === "search" ? "src/SearchWindow/index.ts" : "src/ChatWindow/index.ts",
          formats: ["es", "cjs", "iife"],
          name: "bundle.min.js",
        },
        rollupOptions: {
          external: ["vue"],
          output: {
            globals: {
              vue: "Vue",
            },
          },
        },
      },
    };
  }
};
