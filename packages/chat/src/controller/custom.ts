interface ComponentWithSetup {
  setupElement: () => void;
  key: string;
}

export default async function componentSession() {
  const modules: { [key: string]: ComponentWithSetup } = {};
  try {
    const vueComponents = import.meta.glob("../components/**/index.ts");

    for (const path in vueComponents) {
      const component: ComponentWithSetup = await import(path as string);
      modules[path] = component;

      if (component.setupElement) {
        component.setupElement();
        console.log("注册web组件", component.key);
      }
    }
  } catch (e) {}

  console.log(modules);
  return modules;
}
