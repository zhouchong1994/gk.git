import axios from "axios";

export interface SendMessageParams {
  baseUrl: string;
  anonKey: string;
  message: string;
  inputs?: { [x: string]: any };
  field?: string;
  tweaks?: string;
}

export function parseParams(
  message: string,
  inputs: { [x: string]: any } | undefined,
  field: string | undefined,
  tweaks: string | undefined
) {
  let params: any = { inputs: inputs };
  inputs![field!] = message;
  if (tweaks) {
    params.tweaks = tweaks;
  }
  console.log(params);
  return params;
}

export async function sendMessage({ baseUrl, anonKey, message, inputs, field, tweaks }: SendMessageParams) {
  if (!baseUrl) throw new Error("baseUrl is not allowed");
  const data = parseParams(message, inputs, field, tweaks);
  const prefix = "/api/v1/process/";
  console.log(`${baseUrl + prefix}${anonKey}`);
  let response = axios.post(`${baseUrl + prefix}${anonKey}`, data, {
    headers: { "Content-Type": "application/json" },
  });
  return response;
}

// 流式返回
export async function chatStreamEvent(
  { baseUrl, anonKey, message, inputs, field, tweaks }: SendMessageParams,
  onReceived: (data: any) => void,
  onError: (error: Error) => void
) {
  const data = parseParams(message, inputs, field, tweaks);

  const prefix = "/api/v1/process/";

  const response = (await fetch(`${baseUrl + prefix}${anonKey}`, {
    method: "POST",
    headers: {
      Accept: "text/event-stream",
      "Content-Type": "text/event-stream; charset=utf-8",
    },
    body: JSON.stringify(data),
  }).catch((error) => {
    onError(error);
  })) as Response;
  if (!response.body) {
    throw new Error("ReadableStream not supported in this browser.");
  }

  const reader = response.body.getReader();
  // 逐步处理数据
  const read = async () => {
    const { value, done } = (await reader.read().catch((error: Error) => {
      onError(error);
    })) as { value: Uint8Array; done: boolean };
    if (done) {
      console.log("数据接收完毕");
      return;
    }
    onReceived(value);
    read();
  };
  read();

  return response;
}

// Socket返回
export async function chatStreamSocket(
  { baseUrl, anonKey }: SendMessageParams,
  onReceived: (data: any) => void,
  onError: (event: Event) => void
) {
  // const data = parseParams(message, inputs, field, tweaks)
  const defaultFix = `?user_id=40&api_key=sk-H6rGMGXSD45kqbM2gXSxTKPx0sHCSjBly7q9m4C1oQldVUCM&base_url_name=usflow`;

  const prefix = `/aigc/langflow/flow-chat/`;

  const ws = new WebSocket(`${baseUrl + prefix}${anonKey}${defaultFix}`);
  ws.onopen = () => {
    console.log("WebSocket connection established!");
  };
  ws.onmessage = (event) => {
    const data = JSON.parse(event.data);
    console.log("onmessage", data);
    onReceived(data);
  };
  ws.onclose = (event) => {
    console.log("WebSocket connection onclose!");
  };
  ws.onerror = (ev: Event) => {
    console.log("WebSocket connection onerror!");
    onError(ev);
  };

  return ws;
}
