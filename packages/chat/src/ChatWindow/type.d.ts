export interface Message {
  id?: number;
  text: string;
  isSender: boolean;
  date: number;
  error?: boolean;
  avatar?: string;
  isLoading?: boolean;
  isEnd?: boolean;
}

export interface ResponseMessage {
  files: any;
  intermediate_steps: string;
  is_bot: true;
  message: string;
  type: ResponseMessageType.strat | ResponseMessageType.stream | ResponseMessageType.end;
}
