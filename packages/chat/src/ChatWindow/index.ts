import { defineCustomElement } from "vue";
import VueComponent from "./index.ce.vue";

export const key = "custom-flow-chat";

export const customEl = defineCustomElement(VueComponent);
export const chatComponent = VueComponent;

export function register(tag: string = key) {
  if (!customElements.get(tag)) {
    customElements.define(tag, customEl);
  } else {
    console.error(`自定义元素 ${tag}, 已经注册过了`);
  }
}
