(function(global, factory) {
  typeof exports === "object" && typeof module !== "undefined" ? factory(exports, require("@openxui/button"), require("@openxui/input"), require("@openxui/log")) : typeof define === "function" && define.amd ? define(["exports", "@openxui/button", "@openxui/input", "@openxui/log"], factory) : (global = typeof globalThis !== "undefined" ? globalThis : global || self, factory(global.OpenxuiShared = {}, global.button, global.input, global.log));
})(this, function(exports2, button, input, log) {
  "use strict";
  Object.keys(button).forEach((k) => {
    if (k !== "default" && !Object.prototype.hasOwnProperty.call(exports2, k))
      Object.defineProperty(exports2, k, {
        enumerable: true,
        get: () => button[k]
      });
  });
  Object.keys(input).forEach((k) => {
    if (k !== "default" && !Object.prototype.hasOwnProperty.call(exports2, k))
      Object.defineProperty(exports2, k, {
        enumerable: true,
        get: () => input[k]
      });
  });
  Object.keys(log).forEach((k) => {
    if (k !== "default" && !Object.prototype.hasOwnProperty.call(exports2, k))
      Object.defineProperty(exports2, k, {
        enumerable: true,
        get: () => log[k]
      });
  });
  Object.defineProperty(exports2, Symbol.toStringTag, { value: "Module" });
});
