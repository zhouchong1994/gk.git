import { defineConfig } from 'vite';

export default defineConfig({
  build: {
    lib: {
      entry: './src/index.ts',
      name: 'gkAI',
      fileName: 'gk-ai',
    },

    minify: false,

    rollupOptions: {
      external: [
        /@gk.*/,
        'vue',
      ],

      output: {
      },
    },
  },
});
