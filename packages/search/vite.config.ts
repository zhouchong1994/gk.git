import { loadEnv } from "vite";
import vue from "@vitejs/plugin-vue";
import AutoImport from "unplugin-auto-import/vite";
import Components from "unplugin-vue-components/vite";
import { ElementPlusResolver } from "unplugin-vue-components/resolvers";
import Icons from "unplugin-icons/vite";
import IconsResolver from "unplugin-icons/resolver";
import path from "path";
import type { UserConfig, ConfigEnv } from "vite";
import Unocss from "unocss/vite";
import { createSvgIconsPlugin } from "vite-plugin-svg-icons";
const CWD = process.cwd();
import { resolve } from "node:path";
import VitePluginStyleInject from "vite-plugin-style-inject";
import dts from "vite-plugin-dts";

import {
  transformerVariantGroup,
  transformerDirectives,
  presetAttributify,
  defineConfig,
  presetMini,
  presetUno,
} from "unocss";
export default ({ command, mode }: ConfigEnv): UserConfig => {
  {
    // 环境变量
    return {
      resolve: {
        alias: {
          "@": path.resolve(__dirname, "src"),
        },
      },
      plugins: [
        vue({
          template: {
            compilerOptions: {
              // isCustomElement: (tag: string[]) => {
              //   console.log(tag);
              // },
            },
          },
        }),
        dts({
          entryRoot: "./src",
          outDir: "dist/types",
          copyDtsFiles: true,
        }),
        AutoImport({
          imports: ["vue"],
          resolvers: [
            ElementPlusResolver({
              importStyle: "sass",
            }),
            IconsResolver({
              prefix: "Icon",
            }),
          ],
        }),
        Components({
          resolvers: [
            ElementPlusResolver({
              importStyle: "sass",
            }),
            IconsResolver({
              enabledCollections: ["ep"],
            }),
          ],
        }),
        Icons({
          autoInstall: true,
        }),
        Unocss({
          presets: [presetMini({ dark: "class" }), presetAttributify(), presetUno()],
          transformers: [transformerDirectives(), transformerVariantGroup()],
          shortcuts: {
            "wh-full": "w-full h-full",
            "flex-ac": "flex justify-around items-center",
            "flex-bc": "flex justify-between items-center",
          },
          theme: {},
          rules: [[/^bd-#[0-9a-fA-F]{6}$/, (match) => ({ border: `1px solid ${match[0].slice(3)}` })]],
        }),
        createSvgIconsPlugin({
          // Specify the icon folder to be cached
          iconDirs: [resolve(CWD, "src/assets/icons")],
          // Specify symbolId format
          symbolId: "i-svg-[dir]-[name]",
          customDomId: "_i_svg_dom_", //自定义domId, 避免打包后和其他项目重复造成svg丢失
        }),
        VitePluginStyleInject(),
      ],
      server: {
        host: "0.0.0.0",
        port: 8088,
        proxy: {
          "/api": {
            target: "https://open-test.growknows.cn/",
            changeOrigin: true,
            rewrite: (path) => path.replace(/^\/api/, ""),
          },
        },
      },
      build: {
        target: "esnext",
        minify: "esbuild",
        cssTarget: "chrome79",
        chunkSizeWarningLimit: 2000,
        cssCodeSplit: false,
        lib: {
          entry: "src/index.ts",
          formats: ["es", "cjs", "iife"],
          name: "bundle.min.js",
          fileName: "gk-search",
        },
        rollupOptions: {
          external: ["vue"],
          output: {
            globals: {
              vue: "vue",
            },
          },
        },
      },
    };
  }
};
