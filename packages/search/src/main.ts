import { createApp } from "vue";
import App from "./App.vue";
import "virtual:uno.css";
import "virtual:svg-icons-register";
import "@/style/index.scss";
createApp(App).mount("#app");
