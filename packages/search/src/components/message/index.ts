import MessageItem from "./index.vue";

export type MessageInstance = InstanceType<typeof MessageItem>;
export default MessageItem;
