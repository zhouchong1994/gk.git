import DraggableButton from "./index.vue";

export type ButtonInstance = InstanceType<typeof DraggableButton>;
export default DraggableButton;
