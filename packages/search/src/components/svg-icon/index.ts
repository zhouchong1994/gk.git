import SvgIcon from "./index.vue";

export type SvgIconInstance = InstanceType<typeof SvgIcon>;
export { SvgIcon };
