import { ref, Ref, reactive, nextTick, unref } from "vue";
import { Message, ResponseMessage } from "@/types/type";
import { MessageTypeKey, MessageTypeProp, SocketArkType, StatusTypeProp } from "./enum";

export type HTTPConfigProps = {
  baseUrl?: string;
  prefixUrl?: string;
  anonKey?: string;
  field?: any;
  automatic?: boolean;
};
// export type useChatType = ReturnType<typeof useChat>;

export type useChatType = {
  input: Ref<string>;
  loading: Ref<boolean>;
  offline: Ref<boolean>;
  scrollContainer: Ref<any>;
  addMessage: (message: Message) => void;
  handleInputChange: (e: KeyboardEvent | Event) => void;
  messages: Ref<Message[]>;
  scrollToBottom: () => void;
  handleClear: () => void;
  connectWs: () => void;
  clearMessages: () => void;
  isMobile: () => boolean;
  onCompositionstart: (e: CompositionEvent) => void;
  onCompositionend: (e: CompositionEvent) => void;
  setConfig: (requestConfig: HTTPConfigProps) => void;
  closeWs: () => void;
  updateMessage: (messageRow: Partial<Message>) => void;
};

export const useChat = (options?: { onclose: (info: string) => void }): useChatType => {
  /** The current value of the input */
  const input = ref<string>("");
  /** Whether the API request is in progress */
  const loading = ref<boolean>(false);
  /** Current input status  by h5 api */
  const doing = ref<boolean>(false);
  const offline = ref<boolean>(false);
  /** Current messages in the chat */
  const messages = ref<Message[]>([]);
  /** 当前最近会话的ai应答 */
  const AIMessage = ref<Message | null>(null);
  /** Current scroll parent dom */
  const scrollContainer = ref<HTMLElement | null>(null);

  /** 缓存最近一条未发送的ws任务 */
  const cache = ref<Function | null>();

  let ws = reactive({}) as WebSocket;

  const config = ref<HTTPConfigProps>({
    baseUrl: "",
    prefixUrl: "",
    anonKey: "",
    field: {},
    automatic: true,
  });

  const setConfig = (requestConfig: HTTPConfigProps | any) => {
    config.value = Object.assign(config.value || {}, requestConfig);
  };

  const connectWs = (): void => {
    const { baseUrl, anonKey, prefixUrl } = config.value;
    if (!baseUrl) {
      console.log("[search] - current ws not baseUrl");
      return;
    }

    const url = baseUrl + prefixUrl + anonKey;

    ws = new WebSocket(url);

    ws.onopen = () => {
      console.log("[search] - WebSocket connection open!");

      offline.value = false;

      if (cache.value) {
        console.log("[search] - reconnection executing cache task");
        cache.value?.();
      }
      cache.value = null;
    };
    ws.onmessage = (event) => {
      const data: ResponseMessage = JSON.parse(event.data);
      handleMessage(data);
    };
    ws.onclose = (event: Event) => {
      console.log("[search] - WebSocket connection close!");
      offline.value = true;
      loading.value = false; // 清除发送中状态，再次发送时重连
    };
    ws.onerror = (ev: Event) => {
      console.log("[search] - WebSocket connection error!");
      offline.value = true;
      loading.value = false;
      cache.value = null;
    };
  };

  const closeWs = () => {
    ws.close();
  };

  const onCompositionstart = (e: CompositionEvent) => {
    doing.value = true;
  };

  const onCompositionend = (e: CompositionEvent) => {
    doing.value = false;
  };
  // enter 发送， 按住 shift+ enter 换行
  const handleInputChange = (e: KeyboardEvent | Event) => {
    const { shiftKey } = e as KeyboardEvent;
    if (!shiftKey) {
      e.preventDefault();
    }
    if (shiftKey) return;
    if (doing.value || loading.value) return;

    sendMessage();
  };

  const handleClear = () => {
    input.value = "";
  };

  const handleMessage = (data: ResponseMessage): void => {
    console.log("[search] - ", data);
    const { detailType, type, messages: messagesList, id } = data;

    if (detailType === SocketArkType.online) {
      return;
    }
    if (detailType === SocketArkType.offline) {
      return;
    }

    if (detailType === SocketArkType.startStream) {
      if (!AIMessage.value) {
        addMessage({
          id: MessageTypeKey.AI,
          type: MessageTypeProp.Skeleton,
          text: "",
          isBot: true,
          status: StatusTypeProp.initiate,
        });
      }
    }

    let curMessage = AIMessage.value as Message;

    if (type === SocketArkType.message) {
      curMessage.status = StatusTypeProp.pending;
      curMessage.type = MessageTypeProp.AI;
      curMessage.sn = id;

      const content = messagesList[0];
      if (content) {
        if (content.type === "json") {
          try {
            var resource = atob(content.data);
            curMessage.resource = JSON.parse(resource);
          } catch (error) {}
        } else {
          curMessage.text += content.data;
        }
      }
      scrollToBottom();
    }
    if (type === SocketArkType.endStream || detailType === SocketArkType.endFlowable) {
      if (type === SocketArkType.endStream) curMessage.text = content.data;
      curMessage.type = MessageTypeProp.AI;
      loading.value = false;
      curMessage.status = StatusTypeProp.fulfilled;
      scrollToBottom();
    }
    if (detailType === SocketArkType.logAnswers) {
      curMessage.sn = messagesList[0]?.data;
    }
  };

  const sendMessage = (): void | boolean => {
    const inputVal = input.value;
    if (!inputVal || !inputVal.trim()) return false;

    loading.value = true;

    //每次发送重置当前ai应答实体
    AIMessage.value = null;

    addMessage({
      id: MessageTypeKey.User,
      type: MessageTypeProp.User,
      text: inputVal,
      isBot: false,
      status: StatusTypeProp.fulfilled,
    });

    // add bot chat
    addMessage({
      id: MessageTypeKey.AI,
      type: MessageTypeProp.Skeleton,
      text: "",
      isBot: true,
      status: StatusTypeProp.initiate,
    });

    const params = {
      selfId: config.value.anonKey,
      selfName: "",
      type: "meta",
      detailType: SocketArkType.startFlowable,
      messages: [
        {
          type: "jsonArray",
          data: JSON.stringify([
            {
              ...unref(config.value.field),
              defaultValue: inputVal,
            },
          ]),
        },
      ],
    };
    console.log("params", params);

    // 若允许自动重连，则在发送时检测连接状态
    if (config.value.automatic && (ws.readyState === ws.CLOSED || ws.readyState === ws.CLOSING)) {
      options?.onclose?.("WebSocket connection close");
      cache.value = () => unref(ws)?.send(JSON.stringify(params));
      console.log(cache.value);
    } else {
      unref(ws)?.send(JSON.stringify(params));
    }

    input.value = "";
  };

  /** add message to messages */
  const addMessage = (message: Message) => {
    messages.value.push(message);

    if (message.id === MessageTypeKey.AI && message.type === MessageTypeProp.Skeleton) {
      messages.value.splice(unref(messages).length - 1, 1, message);
      AIMessage.value = message;
    }

    scrollToBottom();
  };

  const clearMessages = () => {
    messages.value = [];
    loading.value = false;
  };

  // 修改问答消息
  const updateMessage = (messageRow: Partial<Message>): void => {
    messages.value = messages.value.map((message) => {
      return message.sn === messageRow.sn ? Object.assign(message, messageRow) : message;
    });
    console.log("updateMessage", messages.value);
  };
  let timer: number | null;
  const scrollToBottom = () => {
    if (timer) {
      clearTimeout(timer);
      timer = null;
    }
    timer = setTimeout(() => {
      const container = scrollContainer.value;
      if (container) {
        // container.scrollTop = container.scrollHeight;
        container.scrollTo({
          top: container?.scrollHeight + 9999, //保证滚动到底部
          behavior: "smooth",
        });
      }
    });
  };

  // 用户代理匹配移动设备
  const isMobile = () => {
    const toMatch = [/Android/i, /webOS/i, /iPhone/i, /iPad/i, /iPod/i, /BlackBerry/i, /Windows Phone/i];
    return toMatch.some((toMatchItem) => {
      return navigator.userAgent.match(toMatchItem);
    });
  };

  return {
    input,
    loading,
    addMessage,
    handleInputChange,
    messages,
    scrollContainer,
    scrollToBottom,
    onCompositionstart,
    onCompositionend,
    isMobile,
    handleClear,
    connectWs,
    offline,
    clearMessages,
    updateMessage,
    setConfig,
    closeWs,
  };
};
