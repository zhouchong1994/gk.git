import { ref } from "vue";

export const useHistory = () => {
  /** The current value of the Visible */
  const historyVisible = ref<boolean>(false);
  const historyList = ref<any>([]);

  const show = () => {
    historyVisible.value = true;
  };
  const hide = () => {
    historyVisible.value = false;
  };

  return {
    historyVisible,
    historyList,
    show,
    hide,
  };
};
