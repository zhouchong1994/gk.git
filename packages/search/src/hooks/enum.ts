// export enum ResponseMessageType {
//   stream = "stream",
//   strat = "strat",
//   end = "end",
//   sources = "sources",
// }
export enum MessageTypeKey {
  User = 0,
  AI = 10000,
}
export enum MessageTypeProp {
  User = "user",
  AI = "ai",
  Skeleton = "ai-skeleton",
}

export enum FeedbackType {
  default = "0", // 默认
  thumb = "1", // 赞
  stomp = "2", // 踩
}

export enum StatusTypeProp {
  initiate = "initiate", //初始化
  pending = "pending", // 进行中
  fulfilled = "fulfilled", // 完成
  interrupt = "interrupt", // 中断
}

export enum SocketArkType {
  offline = "offline",
  online = "online",
  startStream = "startStream",
  message = "message",
  endStream = "endStream",
  endFlowable = "endFlowable",
  logAnswers = "logAnswers",
  startFlowable = "startFlowable",
}
