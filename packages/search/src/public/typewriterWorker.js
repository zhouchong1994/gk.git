/**
 * 打字机效果
 * @description 打字机效果在浏览器切换页签会中断脚本和动画,故启用Worker线程无阻塞运行
 */
class Typewriter {
  constructor() {
    this.INTERVAL = 100
    this._fulfilled = 'fulfilled'
    this._pending = 'pending'

    this.timer = null
    this.isInterrupted = false
    this.status = 'initiate'
  }
  get Fulfilled() {
    return this._fulfilled
  }

  get Pending() {
    return this._pending
  }

  get Status() {
    return this.status
  }

  /**
   * 启动
   * @param content { string }
   * @param callback { function }
   */
  start(content, callback) {
    let index = 0
    const total = content.length
    this.status = this._pending
    this.timer = setInterval(() => {
      const char = content.charAt(index)
      this.status = index < content.length ? this._pending : this._fulfilled
      callback({
        text: char,
        index,
        total,
        status: this.status
      })
      if (this.isInterrupted || index === content.length) {
        clearInterval(this.timer)
        this.timer = null

        return
      }
      ++index
    }, this.INTERVAL)
  }

  /**
   * 中断
   * @description 设计中断而不是清空，意在后期可能会做回到某页面恢复脚本，暂不实现恢复
   */
  interrupt() {
    clearInterval(this.timer)
    this.timer = null
    this.isInterrupted = true
  }
}

const typewriter = new Typewriter()

self.addEventListener('message', (event) => {
  const { eventName, content } = event.data
  console.log('Event', eventName, content.length, typewriter.Status)
  if (eventName === 'start' && typewriter.Status !== typewriter.Pending) {
    if (content.length === 0) {
      self.postMessage({ text: content, index: content.length, total: content.length, status: typewriter.Fulfilled })
      return
    }
    typewriter.start(content, (...args) => {
      self.postMessage(...args)
    })
  } else if (eventName === 'interrupt') {
    typewriter.interrupt()
  }
})
