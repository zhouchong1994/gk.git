import { StatusTypeProp, MessageTypeProp, FeedbackType, SocketArkType } from "@/hooks/enum";

export interface Resource {
  source: string;
  title: string;
  id?: string;
  doc_type?: string;
}

export interface Message {
  id?: number;
  sn?: string;
  text: string;
  isBot: boolean;
  type: MessageTypeProp;
  resource?: Resource[];
  status: StatusTypeProp; // 用于记录当前消息的流式处理状态
  feedbackType?: FeedbackType; // 用于记录当前消息的反馈类型
}
export interface ResponseMessage {
  id?: string;
  selfId?: string;
  messages: any[];
  type: SocketArkType;
  detailType?: string;
  agentNodeId?: string;
}
