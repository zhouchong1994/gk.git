export interface Resource {
  source: string;
  title: string;
  id?: string;
}

export interface Message {
  id?: number;
  text: string;
  isBot: boolean;
  type: "user" | "ai" | "ai-skeleton";
  resource?: Resource[];
  status: "initiate" | "pending" | "fulfilled"; // 用于记录当前消息的流式处理状态
}
export interface ResponseMessage {
  messages: any[];
  type: "offline" | "online" | "startStream" | "message" | "endStream";
  detailType?: string;
  agentNodeId?: string;
}

export enum MessageTypeProp {
  User = "user",
  AI = "ai",
  Skeleton = "ai-skeleton",
}
