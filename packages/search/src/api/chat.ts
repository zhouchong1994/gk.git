
export interface SendMessageParams {
  baseUrl: string;
  anonKey: string;
  message: string;
  collection?: string;
  type?: string;
  fields?: string;
}

// 流式返回
export async function chatStreamEvent(
  { baseUrl, anonKey, message, collection, fields, type }: SendMessageParams,
  onReceived: (data: any) => void,
  onFinish: (data: any) => void,
  onError: (error: Error) => void
) {
  let data: any = {
    api_key: anonKey,
    ds_type: type,
    text: message,
    collection: collection,
    rag: 1
  };
  if (fields) {
    data.fields = fields;
  }
 
  const prefix = "api/aigc/databases/semantic_chat";
  console.log(baseUrl, prefix)
  const response = (await fetch(baseUrl + prefix, {
    method: "POST",
    headers: {
      Accept: "text/event-stream",
      "Content-Type": "text/event-stream; charset=utf-8",
    },
    body: JSON.stringify(data),
  }).catch((error) => {
    onError(error);
  })) as Response;
  if (!response.body) {
    throw new Error("ReadableStream not supported in this browser.");
  }

  const reader = response.body.getReader();
  // 逐步处理数据
  const read = async () => {
    const { value, done } = (await reader.read().catch((error: Error) => {
      onError(error);
    })) as { value: Uint8Array; done: boolean };
    if (done) {
      console.log("数据接收完毕");
      onFinish(value)
      return;
    }
    onReceived(value);
    read();
  };
  read();

  return response;
}

