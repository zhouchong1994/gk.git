import { SearchWindow } from "./SearchWindow";
import MessageItem from "@/components/message";
import { useChat } from "@/hooks/useChat";
import "virtual:uno.css";
import "virtual:svg-icons-register";
import "@/style/index.scss";

console.log("dev", useChat);

export { SearchWindow, useChat, MessageItem };
