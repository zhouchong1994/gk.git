export const searchProps = {
  /**
   * ws 连接url
   */
  hostUrl: {
    type: String,
    default: "",
  },
  /**
   * ws 访问path
   */
  prefixUrl: {
    type: String,
    default: "businessSocket/flowable-socket/",
  },
  /**
   * 发送消息的实体,用户的输入会填充到defaultValue
   */
  field: {
    type: Object,
    default: () => {
      {
        id: "keyword";
        dataType: "string";
        defaultValue: "";
      }
    },
  },
  /**
   * ws 连接会话id
   */
  anonKey: {
    type: String,
    default: "",
  },
  /**
   * ai问答的头像
   */
  botIcon: String,

  /**
   * 输入框的占位符
   */
  placeholder: {
    type: String,
    default: "请输入问题，可通过shift+回车换行",
  },

  /**
   * 自动重连ws
   * default: true
   * ws超时断开后，下次发送消息会触发onclose事件向调用者申请切换新id重新连接
   * 为false时不会触发onclose事件
   */
  automatic: {
    type: Boolean,
    default: true,
  },
  /**
   * 自定义文档来源打开方式
   * default: false
   * 默认a链接单独打开窗口
   */
  customSourceOpenWith: {
    type: Boolean,
    default: false,
  },
};
