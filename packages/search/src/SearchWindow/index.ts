import SearchWindow from "./index.vue";

export type SearchInstance = InstanceType<typeof SearchWindow>;
export { SearchWindow };
