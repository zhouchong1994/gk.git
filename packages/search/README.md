# gk search

A Vue component for gk-chat into your application.

## Project setup

```
npm install --registry=https://registry.npm.org
```

### Installation

```
npm install @gk/search
```

### Usage

```
import { SearchWindow } from "@gk/search"
<SearchWindow base_url='YOUR_URL' anon_key='YOUR_ANON_KEY' />
```

### Usage hooks

```
usage hooks custom ui

import { useChat } from "@gk/search"

const { input handleInputChange, messages, loading } = useChat()

//index.vue
return (
 <div>
   <div>
    {messages.map((msg, i) => <p key={i}>{msg}</p>)}
  </div>
  <div>
    <input v-model={input} />
    <button onClick={handleInputChange}>Send</button>
  </div>
 </div>
)

```
