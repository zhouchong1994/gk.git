# FastAPI-OpenAPI

本文档旨在详细描述一个基于FastAPI框架的OpenAPI开放平台的设计和实现。平台提供了账号注册和登录功能，平台用户可以申请OpenAPI key，用于后面访问LLM（大语言模型）。该平台将允许用户通过API key连接和使用多种大语言模型（openai、azure、文心一言、通义千问等）。平台账号申请的key在调用LLM时，根据使用的token数和不同模型的计费方式，提供了一个计费系统功能。
