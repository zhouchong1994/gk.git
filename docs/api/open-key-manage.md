# OpenAPI Key 申请和管理

## OpenAPI Key 申请

### 请求

```
GET /aigc/key/create_api_key
```

### 请求参数

- api_key_name: string, 申请的api key的别名
- 返回结果:
  - code: int, 200
  - message: string, success
  - data: ApiKeyData, aigc_api_key_user表的数据结构

### 返回结果

- code: int, 200
- message: string, success
- data: ApiKeyData, aigc_api_key_user表的数据结构

```
{
    "code": 200,
    "message": "success",
    "data": {
        "user_id": 20,
        "api_key": "sk-Y8V5eNCjbX5sf4fmfxFecFoTDnPRDTNbpi8l2KU7A0gmfFTE",
        "name": "ww1",
        "is_active": true,
        "total_use": 0,
        "id": 17,
        "update_datetime": "2023-10-07 17:12:06",
        "create_datetime": "2023-10-07 17:12:06"
    }
}
```

## OpenAPI Key 删除

### 请求

```
DELETE /aigc/key/delete_api_key
```

### 请求参数

- user_id: user_id
- api_key_name: string, API key 的名称

### 返回结果

```
{
"code": 200,
"message": "success",
"data": "删除成功"
}
```

## 获取当前用户 API Key

- 获取当前用户申请的所有API Key，或者指定key name的API KEY

### 请求

```
GET /aigc/key/get_api_key
```

### 请求参数

- api_key_name: Optional[string], API key 的名称

### 返回结果

```
{
    "code": 200,
    "message": "success",
    "data": [{
        "user_id": 20,
        "api_key": "sk-Y8V5eNCjbX5sf4fmfxFecFoTDnPRDTNbpi8l2KU7A0gmfFTE",
        "name": "ww1",
        "is_active": true,
        "total_use": 0,
        "id": 17,
        "update_datetime": "2023-10-07 17:12:06",
        "create_datetime": "2023-10-07 17:12:06"
    }]
}
```
