# 基于ingested Data的流式chat 接口

## 请求

```
POST /aigc/databases/ingest_data
```

## 请求参数

- api_key: string，open端的api key
- urls: json, 用于数据提取的各个文档URL、文档id、文档类型（website/pdf/word/execl/ppt...）、文档标签（json）
- target: json, 目标数据库的参数，根据各个向量数据库自带参数进行设置

## 返回结果

```
{
    "code": 200,
    "message": "success",
    "data": ""
}
```
