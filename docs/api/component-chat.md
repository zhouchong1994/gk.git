# Chat API

```
POST /api/v1/process/${flowId}
```

这个组件可以与 GK AI助手聊天，后者根据给定的问题和历史以及项目文档提供回答。我们已经在API的聊天端点中添加了检索器选项-num_chunks params。这允许人们选择检索器将带回多少信息块。

## 创建一个flow id

```
 curl -X POST https://api.mendable.ai/v1/newConversation \
  -H "Content-Type: application/json" \
  -d '{
        "api_key": "YOUR_API_KEY",
      }'
```

<AppChat />

<!-- <web-flow-chat
title='GrowKnows Chat'
subTitle= '随时随地与AI对话'
placeholder= '请输入内容...'
hostUrl= 'wss://open.growknows.cn/api/'
anonKey= '7c9a2ea0-4017-4e17-8b05-c8d2af2bfd23'
appKey= 'sk-H6rGMGXSD45kqbM2gXSxTKPx0sHCSjBly7q9m4C1oQldVUCM'
style="width: 0; position: fixed; z-index: 99999; right: 100px; bottom: 100px"
query= 'username=langflowlangflow&password=langflowlangflow&user_id=40&api_key=sk-H6rGMGXSD45kqbM2gXSxTKPx0sHCSjBly7q9m4C1oQldVUCM&base_url_name=whflow&conversation_id=uid_3232'
/> -->
