import { defineConfig } from "vitepress";
import { mdPlugin } from "./plugins";
import apiConfig from "../configs/api.json";
import blobConfig from "../configs/blob.json";

// 配置参考：https://vitepress.dev/reference/site-config
export default defineConfig({
  base: "/docs/",
  title: "Sara",
  description: "Sara AI",
  ignoreDeadLinks: true,
  // markdown: {
  //   config: (md) => {
  //     md.use(mdPlugin);
  //   },
  // },
  markdown: {
    // 代码块风格
    theme: "material-theme-palenight",
    // theme:'github-light',
    // 代码块显示行数
    lineNumbers: true,
  },
  themeConfig: {
    // 新增 themeConfig.nav 头部导航配置
    // 参考：https://vitepress.dev/reference/default-theme-nav#navigation-links
    logo: { src: "/logo.svg", width: 24, height: 24 },
    nav: [
      {
        text: "指南",
        link: "/guide/what-is-vitepress",
      },
      { text: "文档", link: "/api/", activeMatch: "/index" },
      { text: "文章", link: "/blob/", activeMatch: "/index" },
    ],
    socialLinks: [{ icon: "github", link: "https://github.com/vuejs/vitepress" }],
    outline: {
      // label: "页面导航",
      level: [2, 3],
    },
    // 新增 themeConfig.sidebar 文档章节导航配置
    // 参考：https://vitepress.dev/reference/default-theme-sidebar#multiple-sidebars
    sidebar: {
      "/api/": apiConfig,
      "/blob/": blobConfig,
    },
    footer: {
      message: "基于 MIT 许可发布",
      copyright: `版权所有 © 2023-${new Date().getFullYear()} Sara`,
    },
  },
});
