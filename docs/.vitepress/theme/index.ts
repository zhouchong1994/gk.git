import DefaultTheme from "vitepress/theme";
import { EnhanceAppContext } from "vitepress";
import { register } from "../../configs/aigc-flow-chat.es.js";
register("web-flow-chat");
import chat from "../../components/chat.vue";
import "./common.css";

// 应用组件库的 unocss 预设，配置文件在根目录的 uno.config.ts
import "virtual:uno.css";

export default {
  ...DefaultTheme,
  enhanceApp(ctx: EnhanceAppContext) {
    DefaultTheme.enhanceApp(ctx);

    const { app } = ctx;

    // 注册其他插件、全局组件、provide...

    app.component("AppChat", chat);
  },
};
